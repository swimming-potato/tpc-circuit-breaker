#!/bin/zsh

set -e

if [ $# -ne 1 ]; then
    echo "$0: usage: myscript [configurtion]"
    exit 1
fi

echo "Provided setting: \n\t Condfiguration: $1 "


dotnet publish --configuration "$1"

echo "Copy imageservice to docker location...."
if [ -d ../Docker/edu.tpc.circuitbreaker/imageservice/bin ]; then
    \rm -rf ../Docker/edu.tpc.circuitbreaker/imageservice/bin
fi

cp -r ./Edu.Tpc.CircuitBreaker.ImageService/bin/$1/netcoreapp2.2/publish ../Docker/edu.tpc.circuitbreaker/imageservice/bin

echo "Copy feed to docker location...."
if [ -d ../Docker/edu.tpc.circuitbreaker/indexer/bin ]; then
    \rm -rf ../Docker/edu.tpc.circuitbreaker/indexer/bin
fi

cp -r ./Edu.Tpc.CircuitBreaker.Indexer/bin/$1/netcoreapp2.2/publish ../Docker/edu.tpc.circuitbreaker/indexer/bin

echo "Copy imageloader to docker location...."
if [ -d ../Docker/edu.tpc.circuitbreaker/proxy/bin ]; then
    \rm -rf ../Docker/edu.tpc.circuitbreaker/proxy/bin
fi

cp -r ./Edu.Tpc.CircuitBreaker.ImageProxy/bin/$1/netcoreapp2.2/publish ../Docker/edu.tpc.circuitbreaker/proxy/bin

echo "Copy persentation to docker location...."
if [ -d ../Docker/edu.tpc.circuitbreaker/webapp/bin ]; then
    \rm -rf ../Docker/edu.tpc.circuitbreaker/webapp/bin
fi

cp -r ./Edu.Tpc.CircuitBreaker.Presentation/bin/$1/netcoreapp2.2/publish ../Docker/edu.tpc.circuitbreaker/webapp/bin
