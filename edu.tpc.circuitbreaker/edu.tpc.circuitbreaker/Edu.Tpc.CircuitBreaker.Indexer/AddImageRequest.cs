using System;

namespace Edu.Tpc.CircuitBreaker.Indexer
{
    public class AddImageRequest
    {
        public string Title { get; set; }
        
        public string Content { get; set; }

        public string Link { get; set; }
        
        public DateTime PublishDate { get; set; }
    }
}