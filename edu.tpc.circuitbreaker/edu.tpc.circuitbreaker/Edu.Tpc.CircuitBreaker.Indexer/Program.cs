﻿using System;

using System.Runtime.Loader;


namespace Edu.Tpc.CircuitBreaker.Indexer
{
    class Program
    {
        private static ReadImageAtomService _service;
        
        static void Main(string[] args)
        {
            Console.WriteLine("Initiating feed loader...");
            
            _service = new ReadImageAtomService();
            
            Console.WriteLine("Starting service...");
            _service.Start();

            AssemblyLoadContext.Default.Unloading += SigTermEventHandler;
            Console.CancelKeyPress += CancelHandler;

            while (true) {
                System.Console.Read();
            };
            
        }
        
        private static void SigTermEventHandler(AssemblyLoadContext obj)
        {
            Console.WriteLine("Unloading...");
            _service.Stop();
        }
        private static void CancelHandler(object sender, ConsoleCancelEventArgs e)
        {
            Console.WriteLine("Exiting...");
            _service.Stop();
        }

        
    }
}