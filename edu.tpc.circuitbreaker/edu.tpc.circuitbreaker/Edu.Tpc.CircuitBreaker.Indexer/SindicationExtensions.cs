using System.Linq;
using Microsoft.SyndicationFeed;

namespace Edu.Tpc.CircuitBreaker.Indexer
{
    public static class SindicationExtensions
    {
        public static ImageItem ConvertToImageItem(this ISyndicationItem item)
        {
            string id = item.Id.Replace("http://9gag.com/gag/","image:9gag:");
            return new ImageItem(item.Title,
                item.Links.FirstOrDefault().Uri.AbsoluteUri,
                id,
                item.Description,
                item.Published.UtcDateTime
              );
        }
    }
}