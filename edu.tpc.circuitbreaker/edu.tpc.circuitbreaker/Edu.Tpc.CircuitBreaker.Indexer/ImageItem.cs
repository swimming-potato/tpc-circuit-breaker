using System;

namespace Edu.Tpc.CircuitBreaker.Indexer
{
    public class ImageItem
    {
        public ImageItem(string title,string link, string id, string content,DateTime publishDate)
        {
            Title = title;
            Link = link;
            Id = id;
            Content = content;
            PublishDate = publishDate;
        }
        
        public string Title { get; set; }
        
        public string Link { get; set; }
        
        public string Id { get; set; }
        
        public string Content { get; set; }
        
        public DateTime PublishDate { get; set; }
    }
}