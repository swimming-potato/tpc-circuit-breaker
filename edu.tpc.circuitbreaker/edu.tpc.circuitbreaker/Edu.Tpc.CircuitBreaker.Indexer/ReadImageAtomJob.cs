using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Configuration;
using Microsoft.SyndicationFeed;
using Microsoft.SyndicationFeed.Atom;
using Newtonsoft.Json;
using Quartz;

namespace Edu.Tpc.CircuitBreaker.Indexer
{
    
    
    [DisallowConcurrentExecution]
    public class ReadImageAtomJob : IJob
    {
        public Task Execute(IJobExecutionContext context)
        {
            try
            {
                Console.WriteLine("Starting 'Read image atom' job...");

                var builder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);


                IConfigurationRoot config = builder.Build();

                var feedConfig = new FeedConfiguration();
                config.GetSection("FeedConfiguration").Bind(feedConfig);

                Console.WriteLine($"Configuration...\n\t ImageServiceHost = {feedConfig.ImageWebApiUrl} " +
                                  $"\n\t AtomFeed = {feedConfig.AtomFeed}");

                HttpClient client = new HttpClient();
                client.Timeout = new TimeSpan(0, 2, 0);
                return LoadImages(client, feedConfig);
            }
            catch (Exception e)
            {
                Console.Error.Write("Reading atom feed...ERROR \n" + e.ToString());
            }
            
            return Task.CompletedTask;
            
        }
        
        private async Task LoadImages(HttpClient client,FeedConfiguration configuration)
        {
            var rssImageItems = new List<ImageItem>();
            Console.WriteLine("Reading atom feed...");
            Stream feedTask = await client.GetStreamAsync(configuration.AtomFeed);
            
            using (var xmlReader = XmlReader.Create(feedTask, new XmlReaderSettings(){Async = true}))
            {
                var feedReader = new AtomFeedReader(xmlReader);
                while (await feedReader.Read())
                {
                    if (feedReader.ElementType == SyndicationElementType.Item)
                    {
                        ISyndicationItem item = await feedReader.ReadItem();
                        rssImageItems.Add(item.ConvertToImageItem());
                    }
                }
            }
            Console.WriteLine($"Reading atom feed...Done. Number of images {rssImageItems.Count}");
            var tasks = new List<Task>(rssImageItems.Count);
            foreach (var image in rssImageItems)
            {
                tasks.Add(putImage(client,configuration,image));
            }

            Console.WriteLine("Waiting till sending image will complete...");
            await Task.WhenAll(tasks);
        }

        private async Task<HttpResponseMessage> putImage(HttpClient client,FeedConfiguration configuration,ImageItem image)
        {
            try
            {
                
                string url = configuration.ImageWebApiUrl + "/images/" + image.Id;
                Console.WriteLine($"Sending image id = {image.Id} to {url}");
                var body = new AddImageRequest()
                {
                    Content = image.Content,
                    Link = image.Link,
                    Title = image.Title,
                    PublishDate = image.PublishDate
                };
                var json = JsonConvert.SerializeObject(body);
                var result = await client.PutAsync(url,
                    new StringContent(json, Encoding.UTF8, "application/json"));

                Console.WriteLine($"Sending image id = {image.Id}...Result {result.StatusCode}");

                return result;
            }
            catch (Exception e)
            {
                Console.Error.Write("Reading atom feed...ERROR \n" + e.ToString());
                throw;
            }
        }
    }
}