using System;
using System.Collections.Specialized;
using System.IO;
using Microsoft.Extensions.Configuration;
using Quartz;
using Quartz.Impl;

namespace Edu.Tpc.CircuitBreaker.Indexer
{
    public class ReadImageAtomService
    {
        private readonly IScheduler _scheduler;
        
        public ReadImageAtomService()
        {
            NameValueCollection props = new NameValueCollection
            {
                { "quartz.serializer.type", "binary" },
                { "quartz.scheduler.instanceName", "MyScheduler" },
                { "quartz.jobStore.type", "Quartz.Simpl.RAMJobStore, Quartz" },
                { "quartz.threadPool.threadCount", "3" }
            };
            StdSchedulerFactory factory = new StdSchedulerFactory(props);
            _scheduler = factory.GetScheduler().ConfigureAwait(false).GetAwaiter().GetResult();
        }
        
        public void Start()
        {

            Console.WriteLine("Reading configuration...");

            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
                

            IConfigurationRoot config = builder.Build();
            
            var feedConfig = new FeedConfiguration();
            config.GetSection("FeedConfiguration").Bind(feedConfig);

            Console.WriteLine($"Loaded configuration...\n\t ImageServiceHost = {feedConfig.ImageWebApiUrl} " +
                              $"\n\t AtomFeed = {feedConfig.AtomFeed}");
            
            _scheduler.Start().ConfigureAwait(false).GetAwaiter().GetResult();

            Console.WriteLine("Scheduling job...");
            ScheduleJobs();
            Console.WriteLine("Scheduling job...done");
        }
        
        public void ScheduleJobs()
        {
            IJobDetail job = JobBuilder.Create<ReadImageAtomJob>()
                .WithIdentity("image", "feed")
                .Build();

            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity("trigger1", "group1")
                .StartNow()
                .WithSimpleSchedule(x => x
                    .WithIntervalInMinutes(2)
                    .RepeatForever())
                .Build();

            // Tell quartz to schedule the job using our trigger
            _scheduler.ScheduleJob(job, trigger).ConfigureAwait(false).GetAwaiter().GetResult();
        }
        public void Stop()
        {
            Console.WriteLine("Stopping job...");
            _scheduler.Shutdown().ConfigureAwait(false).GetAwaiter().GetResult();
        }
    }
}