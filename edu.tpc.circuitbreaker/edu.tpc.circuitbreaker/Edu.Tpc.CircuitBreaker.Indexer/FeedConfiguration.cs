namespace Edu.Tpc.CircuitBreaker.Indexer
{
    public class FeedConfiguration
    {
        public string ImageWebApiUrl { get; set; }
        public string AtomFeed { get; set; }
    }
}