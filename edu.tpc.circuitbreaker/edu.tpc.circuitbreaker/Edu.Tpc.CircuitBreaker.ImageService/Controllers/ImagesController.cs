﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Serialization.Json;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Edu.Tpc.CircuitBreaker.ImageService.Configuration;
using Edu.Tpc.CircuitBreaker.ImageService.Model;
using Edu.Tpc.CircuitBreaker.ImageService.Model.Entity;
using Edu.Tpc.CircuitBreaker.ImageService.Service;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Serilog;

namespace Edu.Tpc.CircuitBreaker.ImageService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImagesController : ControllerBase
    {


        private IImageLoaderConfiguration _configuration;
        private IImageService _imageService;
        
        public ImagesController(IImageService service, IImageLoaderConfiguration configuration)
        {
            _imageService = service;
            _configuration = configuration;
                
        }
        
        // GET api/values/5
        [HttpGet]
        public async Task<ActionResult<ImageOfDayResponse>> Get()
        {
            try
            {
                string xMessageId;

                Log.Information("Receiving request for the best image... ");

                if (Request.Headers.ContainsKey("x-message-id"))
                    xMessageId = Request.Headers["x-message-id"];
                else
                    xMessageId = Guid.NewGuid().ToString();


                var image = _imageService.GetBestImage();

                
                Log.Information($"Selected image: {image?.Id}");
                

                if (image != null)
                {
                    
                    TheBestImageResponse imageContent = null;
                    
                    using (var client = new HttpClient())
                    {
                        Log.Information($"Downloading image: {image.Id}");
                        client.DefaultRequestHeaders.Add("x-message-id", xMessageId);
                        string url = _configuration.Url + "/api/images/" + image.Id;
                        
                        Log.Information($"Using url: {url}");
                        var result = await client.GetAsync(url);

                         Log.Information($"Download image....{result.StatusCode}");
                        if (result.IsSuccessStatusCode)
                        {
                            imageContent = JsonConvert.DeserializeObject<TheBestImageResponse>(
                                await result.Content.ReadAsStringAsync());
                        }
                        else
                        {
                            Log.Error($"Fail to download image....{result.StatusCode}");
                            return StatusCode((int) HttpStatusCode.InternalServerError);
                        }
                    }
                    
                    if (imageContent != null)
                    {
                        return new ImageOfDayResponse()
                        {
                            Content = image.Content,
                            Id = image.Id,
                            Link = imageContent.Link,
                            Title = image.Title,
                            Body = imageContent.Body

                        };

                    }
                }

                Log.Warning($"Selected image not found!!!");
                return NotFound();
            }
            catch (Exception e)
            {
                Log.Error($"Failed to get image. Reason: {e.Message}",e);
                throw;
            }
        }
        

        // PUT api/images/5
        [HttpPut("{id}")]
        public IActionResult Put(string id, [FromBody] AddImageRequest request)
        {
            try
            {
                Log.Information("Receiving request to put new image... ");

                TheBestImage image = new TheBestImage()
                {
                    Id = id,
                    Content = request.Content,
                    Title = request.Title,
                    PublishedDate = request.PublishDate
                };

                var task = _imageService.AddImageAsync(image);

                task.Wait();
                Log.Information("Receiving request to put new image...Done ");

                return NoContent();
            }
            catch (Exception e)
            {
                Log.Error($"Failed to add image. Reason: {e.Message}",e);
                throw;
            }
        }
        
    }
}