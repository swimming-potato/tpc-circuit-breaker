using System;

namespace Edu.Tpc.CircuitBreaker.ImageService.Model
{
    public class AddImageRequest
    {
        public string Title { get; set; }
        
        public string Content { get; set; }

        public string Link { get; set; }
        
        public DateTime PublishDate { get; set; }
    }
}