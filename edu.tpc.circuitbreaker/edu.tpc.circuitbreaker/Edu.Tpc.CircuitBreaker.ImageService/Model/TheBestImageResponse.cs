namespace Edu.Tpc.CircuitBreaker.ImageService.Model
{
    public class TheBestImageResponse
    {
        public string Body { get; set; }
    
        public string Link { get; set; }
    }
}