using System;

namespace Edu.Tpc.CircuitBreaker.ImageService.Model.Entity
{
    public class TheBestImage
    {
        public string Title { get; set; }
        
        public string Content { get; set; }

        public string Id { get; set; }
        
        public DateTime PublishedDate { get; set; }
    }
}