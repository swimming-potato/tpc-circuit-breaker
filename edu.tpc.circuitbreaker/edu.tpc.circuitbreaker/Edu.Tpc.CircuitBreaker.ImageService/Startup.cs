﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Edu.Tpc.CircuitBreaker.ImageService.Configuration;
using Edu.Tpc.CircuitBreaker.ImageService.Controllers;
using Edu.Tpc.CircuitBreaker.ImageService.Model;
using Edu.Tpc.CircuitBreaker.ImageService.Service;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Edu.Tpc.CircuitBreaker.ImageService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<ImageDatabaseConfiguration>(
                Configuration.GetSection(nameof(ImageDatabaseConfiguration)));
            
            services.Configure<ImageLoaderConfiguration>(
                Configuration.GetSection(nameof(ImageLoaderConfiguration)));

            services.AddSingleton<IImageDatabaseConfiguration>(sp =>
                sp.GetRequiredService<IOptions<ImageDatabaseConfiguration>>().Value);
            
            services.AddSingleton<IImageLoaderConfiguration>(sp =>
                sp.GetRequiredService<IOptions<ImageLoaderConfiguration>>().Value);

            services.AddSingleton<IImageService,Service.ImageService>();
            
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddHealthChecks()
                .AddCheck<ImageMongoCollectionHealthCheck>("mongoCheck");
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                
            }

            app.UseHealthChecks("/health");
            app.UseMvc();
        }
    }
}