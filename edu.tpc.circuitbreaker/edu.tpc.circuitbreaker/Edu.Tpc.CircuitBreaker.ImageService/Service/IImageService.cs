using System.Threading.Tasks;
using Edu.Tpc.CircuitBreaker.ImageService.Model.Entity;

namespace Edu.Tpc.CircuitBreaker.ImageService.Service
{
    public interface IImageService
    {
        Task AddImageAsync(TheBestImage image);

        TheBestImage GetBestImage();

        Task<bool> IsHealthy();
    }
    
}