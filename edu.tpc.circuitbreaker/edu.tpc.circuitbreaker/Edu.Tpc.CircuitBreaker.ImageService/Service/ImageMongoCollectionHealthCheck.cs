using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace Edu.Tpc.CircuitBreaker.ImageService.Service
{
    public class ImageMongoCollectionHealthCheck : IHealthCheck
    {
        private IImageService _imageService;
        
        public ImageMongoCollectionHealthCheck(IImageService service)
        {
            _imageService = service;
        }
        
        public Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = new CancellationToken())
        {

            var result = _imageService.IsHealthy().GetAwaiter().GetResult();
            

            if (result)
            {
                return Task.FromResult(HealthCheckResult.Healthy());
            }
            else
            {
                return Task.FromResult(HealthCheckResult.Unhealthy("Mongo is not up"));
            }
            
        }
    }
}