using System;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Edu.Tpc.CircuitBreaker.ImageService.Configuration;
using Edu.Tpc.CircuitBreaker.ImageService.Model.Entity;
using MongoDB.Bson;
using MongoDB.Driver;
using Serilog;

namespace Edu.Tpc.CircuitBreaker.ImageService.Service
{
    public class ImageService : IImageService
    {
        private readonly IMongoCollection<TheBestImage> _images;
        private static RNGCryptoServiceProvider rngCsp = new RNGCryptoServiceProvider();

        public ImageService(IImageDatabaseConfiguration configuration)
        {
            Log.Information("Connecting to mongo service...");
            var client = new MongoClient(configuration.ConnectionString);
            var database = client.GetDatabase(configuration.DatabaseName);

            _images = database.GetCollection<TheBestImage>(configuration.CollectionName);
            Log.Information("Connecting to mongo service...done");
        }


        public async Task AddImageAsync(TheBestImage image)
        {
            var document = new BsonDocument
            {
                { "_id", image.Id },
            };
            
            var entity = await _images.Find(document).FirstOrDefaultAsync();

            if (entity == null)
                 await _images.InsertOneAsync(image);
            
        }

        private TheBestImage SelectBestImage()
        {
            // pseudo best image look up algorithm
            string[] keyword = {"cat", "banana", "boy", "girls", "best", "weekend", "want", "love"};
            
            byte[] randomNumber = new byte[1];
            rngCsp.GetBytes(randomNumber);
            int i = randomNumber[0] % 8;

            
            Log.Information($"Selecting image using: {keyword[i]}");

            var filter = new BsonDocument {{"Title", new BsonDocument {{"$regex", keyword[i]}, {"$options", "i"}}}};
            
            IAsyncCursor<TheBestImage> cursor = _images.Find(filter).ToCursor();
            
            TheBestImage result = null;
            
            rngCsp.GetBytes(randomNumber);
            i = randomNumber[0];
            Log.Information($"Selecting image #{i}");

            foreach (var theBestImage in cursor.ToEnumerable())
            {
                result = theBestImage;

                if (i == 0)
                    break;

                i--;


            }
            
            return result;
        }
        
        public TheBestImage GetBestImage()
        {
            int maxItr = 3;
            TheBestImage image = null;
            do
            {
                image = SelectBestImage();
                maxItr--;
            } while (image == null && maxItr > 0);

            if(image == null)
                Log.Information("Image not found!!!");

            return image;
        }

        public async Task<bool> IsHealthy()
        {
            try
            {
                await _images.Find(FilterDefinition<TheBestImage>.Empty).FirstAsync();
                return true;
            }
            catch (Exception e)
            {
                Log.Error("Error during health check....",e);
                return false;
            }
        }
    }
}