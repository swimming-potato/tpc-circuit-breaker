namespace Edu.Tpc.CircuitBreaker.ImageService.Configuration
{
    public interface IImageDatabaseConfiguration
    {
        string ConnectionString { get; set; }
        
        string CollectionName { get; set; }
        
        string DatabaseName { get; set; }
    }
}