namespace Edu.Tpc.CircuitBreaker.ImageService.Configuration
{
    public interface IImageLoaderConfiguration
    {
        string Url { get; set; }
    }
}