using Edu.Tpc.CircuitBreaker.ImageService.Model;

namespace Edu.Tpc.CircuitBreaker.ImageService.Configuration
{
    public class ImageDatabaseConfiguration : IImageDatabaseConfiguration
    {
        public string ConnectionString { get; set; }
        public string CollectionName { get; set; }
        public string DatabaseName { get; set; }
    }
}