namespace Edu.Tpc.CircuitBreaker.ImageService.Configuration
{
    public class ImageLoaderConfiguration : IImageLoaderConfiguration
    {
        public string Url { get; set; }
    }
}