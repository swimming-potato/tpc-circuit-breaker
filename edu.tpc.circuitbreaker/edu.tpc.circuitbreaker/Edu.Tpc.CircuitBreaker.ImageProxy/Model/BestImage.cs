namespace Edu.Tpc.CircuitBreaker.ImageProxy.Model
{
    public class BestImage
    {
        public string Body { get; set; }
        
        public string Link { get; set; }
    }
}