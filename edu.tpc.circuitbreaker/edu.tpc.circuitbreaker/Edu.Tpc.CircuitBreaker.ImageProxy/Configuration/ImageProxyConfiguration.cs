using System.Collections.Generic;

namespace Edu.Tpc.CircuitBreaker.ImageProxy.Configuration
{
    public class ImageProxyConfiguration : IImageProxyConfiguration
    {
        public ImageProxyConfiguration()
        {
            TagMapping = new Dictionary<string, string>();
        }
        public Dictionary<string, string> TagMapping { get; set; }
    }
}