using System.Collections.Generic;

namespace Edu.Tpc.CircuitBreaker.ImageProxy.Configuration
{
    public interface IImageProxyConfiguration
    {
        Dictionary<string, string> TagMapping { get; set; }
    }
}