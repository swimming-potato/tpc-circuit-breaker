﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Edu.Tpc.CircuitBreaker.ImageProxy.Configuration;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Edu.Tpc.CircuitBreaker.ImageProxy
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<ImageProxyConfiguration>(Configuration.GetSection(nameof(ImageProxyConfiguration)));
            services.AddSingleton<IImageProxyConfiguration>((sp) =>
            {
                var config = sp.GetRequiredService<IOptions<ImageProxyConfiguration>>().Value;
                Dictionary<string,string> formatted = new Dictionary<string, string>();
                foreach (var keyValue in config.TagMapping)
                {
                    //ugly but effective replacement o - to :. ':' is not allowed character in key
                    formatted.Add(keyValue.Key.Replace("-",":"),keyValue.Value);
                }

                config.TagMapping = formatted;

                return config;
            });
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }
    }
}