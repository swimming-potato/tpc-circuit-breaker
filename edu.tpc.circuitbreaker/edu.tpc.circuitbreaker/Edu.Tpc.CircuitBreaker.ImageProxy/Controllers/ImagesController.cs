﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Edu.Tpc.CircuitBreaker.ImageProxy.Configuration;
using Edu.Tpc.CircuitBreaker.ImageProxy.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Serilog;

namespace Edu.Tpc.CircuitBreaker.ImageProxy.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImagesController : ControllerBase
    {

        private readonly IImageProxyConfiguration _configuration;

        public ImagesController(IImageProxyConfiguration configuration)
        {
            _configuration = configuration;
           Log.Information($"Reading configuration: {_configuration.TagMapping}");
        
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<ActionResult<BestImage>> Get(string id)
        {
            byte[] content = null;
            try{
            Log.Information($"Reading image {id}...");

                string url = null;

                foreach (var keyValuePair in _configuration.TagMapping)
                {
                    if (id.Contains(keyValuePair.Key))
                    {
                        url = id.Replace(keyValuePair.Key, keyValuePair.Value + "/");
                    }
                }


                if (string.IsNullOrWhiteSpace(url))
                {
                    Log.Error($"Could not find url for {url}");
                    return StatusCode((int) HttpStatusCode.InternalServerError,$"Could not find url mapping for {id}");
                }
                using (var client = new HttpClient())
                {
                    client.Timeout = new TimeSpan(0,1,0);

                    Log.Information($"Downloading resource from {url}...");
                    using (var result = await client.GetAsync(url))
                    {
                        Log.Information($"Download image....{result.StatusCode}");
                        if (result.IsSuccessStatusCode)
                        {
                            content = await result.Content.ReadAsByteArrayAsync();
                        }
                        else
                        {
                            Log.Error($"Could not find image for {result.StatusCode}");
                            return StatusCode((int) HttpStatusCode.InternalServerError,$"Could not find image for {result.StatusCode}");
                        }

                    }
                }

                return new BestImage()
                {
                    Body = Convert.ToBase64String(content),
                    Link =  url
                };
            }catch(Exception e){
                return StatusCode((int)HttpStatusCode.InternalServerError,e.Message);
            }
        }

    }
}