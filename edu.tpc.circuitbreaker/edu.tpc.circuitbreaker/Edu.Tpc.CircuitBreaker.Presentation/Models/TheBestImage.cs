namespace Edu.Tpc.CircuitBreaker.Presentation.Models
{
    public class TheBestImage
    {
        public string Title { get; set; }
        
        public string Content { get; set; }

        public string Link { get; set; }
    }
}