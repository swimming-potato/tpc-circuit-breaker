namespace Edu.Tpc.CircuitBreaker.Presentation.Model
{
    public class ImageOfDayResponse
    {
        public string Title { get; set; }
        
        public string Content { get; set; }

        public string Link { get; set; }
        
        public string Id { get; set; }
        
        public string Body { get; set; }
    }
}