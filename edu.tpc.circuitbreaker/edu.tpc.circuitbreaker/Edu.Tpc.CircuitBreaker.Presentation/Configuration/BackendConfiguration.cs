using System.Security.Policy;

namespace Edu.Tpc.CircuitBreaker.Presentation.Configuration
{
    public class BackendConfiguration : IBackendConfiguration
    {
        public string ImageServiceUrl { get; set; }
    }
}