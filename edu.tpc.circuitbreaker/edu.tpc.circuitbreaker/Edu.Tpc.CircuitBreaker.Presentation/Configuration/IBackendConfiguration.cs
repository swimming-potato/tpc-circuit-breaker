namespace Edu.Tpc.CircuitBreaker.Presentation.Configuration
{
    public interface IBackendConfiguration
    { 
        string ImageServiceUrl { get; set; }
    }
}