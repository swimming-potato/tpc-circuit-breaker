﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Edu.Tpc.CircuitBreaker.Presentation.Configuration;
using Edu.Tpc.CircuitBreaker.Presentation.Service;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Polly;
using Polly.Extensions.Http;
using Polly.Timeout;
using Serilog;

namespace Edu.Tpc.CircuitBreaker.Presentation
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<BackendConfiguration>(
                Configuration.GetSection(nameof(BackendConfiguration)));

            services.AddSingleton<IBackendConfiguration>(sp =>
                sp.GetRequiredService<IOptions<BackendConfiguration>>().Value);
            
            services.AddSingleton<IImageService,ImageService>();

            
            var noOpPolicy = Policy.NoOpAsync().AsAsyncPolicy<HttpResponseMessage>();
            
            var circuitBreaker = HttpPolicyExtensions
                .HandleTransientHttpError()
                .Or<TimeoutRejectedException>()
                .Or<TaskCanceledException>()
                .Or<HttpRequestException>()
                .CircuitBreakerAsync(2, TimeSpan.FromMinutes(5),
                    (result, span) => {
                        Log.Fatal("Breaking the circuit");
                    },
                    () =>
                    {
                        Log.Information("Resetting the circuit");
                    },
                    () =>
                    {
                        Log.Information("Probing the circuit");
                    });
            
            services.AddHttpClient<IImageService, ImageService>()
                .AddPolicyHandler(GetRetryPolicy())
                .AddPolicyHandler( request => request.Headers.Contains("x-circuit-breaker") ? circuitBreaker : noOpPolicy);

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        static IAsyncPolicy<HttpResponseMessage> GetRetryPolicy()
        {
            Random jitterer = new Random();
            return HttpPolicyExtensions.HandleTransientHttpError()
                .OrResult(response => (int) response.StatusCode == 429)
                .Or<TimeoutRejectedException>()
                .Or<TaskCanceledException>()
                .WaitAndRetryAsync(2,
                    retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)) // exponential back-off: 2, 4,  etc
                                    + TimeSpan.FromMilliseconds(jitterer.Next(0,1000)) // plus some jitter: up to 1 second
                    , onRetry: (outcome, timespan, retryAttempt, context)
                        =>
                    {
                        Log.Warning("Delaying for {delay}ms, then making retry {retry}.", 
                            timespan.TotalMilliseconds, retryAttempt);
                    });
        }

        static IAsyncPolicy<HttpResponseMessage> GetTimeoutPolicy()
        {
            return Policy.TimeoutAsync<HttpResponseMessage>(10);
        }
        
        static IAsyncPolicy<HttpResponseMessage> GetCircuitBreakerPolicy()
        {
            return HttpPolicyExtensions
                .HandleTransientHttpError()
                .Or<TimeoutRejectedException>()
                .Or<TaskCanceledException>()
                .Or<HttpRequestException>()
                .CircuitBreakerAsync(2, TimeSpan.FromMinutes(5),
                    (result, span) => {
                        Log.Fatal("Breaking the circuit");
                    },
                    () =>
                    {
                        Log.Information("Resetting the circuit");
                    },
                    () =>
                    {
                        Log.Information("Probing the circuit");
                    });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}