using System;
using System.Net.Http;
using Edu.Tpc.CircuitBreaker.Presentation.Configuration;
using Edu.Tpc.CircuitBreaker.Presentation.Model;
using Edu.Tpc.CircuitBreaker.Presentation.Models;
using Newtonsoft.Json;
using Serilog;

namespace Edu.Tpc.CircuitBreaker.Presentation.Service
{
    public class ImageService : IImageService
    {
        private IBackendConfiguration _configuration;
        private HttpClient _client;

        public ImageService(IBackendConfiguration configuration,HttpClient client)
        {
            _configuration = configuration;
            _client = client;
        }
        
        public TheBestImage GetImage(string name, bool useCircuitBreaker)
        {
            ImageOfDayResponse image = null;
            Log.Information("Looking for the best image...");

                
            string url = _configuration.ImageServiceUrl + "/api/images/";
                    
            Log.Information($"Using url: {url}");

            HttpResponseMessage result; 
            using (var requestMessage =
                new HttpRequestMessage(HttpMethod.Get, url))
            {
                
                requestMessage.Headers.Add("x-circuit-breaker", useCircuitBreaker.ToString());

                Log.Information("Is enabled on circuit breaker on request: " + requestMessage.Headers.Contains("x-circuit-breaker"));
                
                
                result = _client.SendAsync(requestMessage).GetAwaiter().GetResult();
            }

            if (result.IsSuccessStatusCode)
            {
                image = JsonConvert.DeserializeObject<ImageOfDayResponse>(
                     result.Content.ReadAsStringAsync().GetAwaiter().GetResult());
            }
            else
            {
                throw new Exception($"Fail to download image....{result.StatusCode}");
            }


            if (image != null)
            {
                return new TheBestImage()
                {
                    Title = image.Title,
                    Content = image.Content,
                    Link = image.Link
                };
            }
            else
            {
                return null;
            }
        }
    }
}