using System.Threading.Tasks;
using Edu.Tpc.CircuitBreaker.Presentation.Models;

namespace Edu.Tpc.CircuitBreaker.Presentation.Service
{
    public interface IImageService
    {
        TheBestImage GetImage(string name,bool useCircuitBreaker);
    }
}