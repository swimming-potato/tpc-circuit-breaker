using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Edu.Tpc.CircuitBreaker.Presentation.Configuration;
using Edu.Tpc.CircuitBreaker.Presentation.Model;
using Edu.Tpc.CircuitBreaker.Presentation.Models;
using Edu.Tpc.CircuitBreaker.Presentation.Service;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Polly.CircuitBreaker;
using Serilog;

namespace Edu.Tpc.CircuitBreaker.Presentation.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImagesController : ControllerBase
    {
        private IBackendConfiguration _configuration;
        private IImageService _service;
        
        public ImagesController(IBackendConfiguration configuration, IImageService service)
        {
            _configuration = configuration;
            _service = service;
        }
        
        [HttpGet]
        public async Task<ActionResult<TheBestImage>> Get()
        {
            bool useCircuitBreaker = true;

            Log.Information("Receiving request for the best image... ");

            if (Request.Headers.ContainsKey("x-circuit-breaker")) 
                bool.TryParse(Request.Headers["x-circuit-breaker"],out useCircuitBreaker);

            try
            {
                TheBestImage image = _service.GetImage("gang", useCircuitBreaker);

                if (image == null)
                    return NotFound();
                else
                    return image;
            }
            catch (BrokenCircuitException e)
            {
                return new TheBestImage()
                {
                    Content =
                        "<img src=\"https://pics.me.me/youremy-hero-makeameme-org-youre-my-hero-happy-homer-54167767.png\">",
                    Title = "It is safe",
                    Link =
                        "https://me.me/i/youremy-hero-makeameme-org-youre-my-hero-happy-homer-b96de2a8c7b449ba8c1d0b10260f82b2"
                };
            }
            catch (Exception e)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError,
                    $"Error: {e.Message}");
            }
           
        }
    }
    

}