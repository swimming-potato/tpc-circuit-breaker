"use strict";

angular.module('app', [
    'ngSanitize'
]);

var lab = angular.module('lab', []);

lab.controller('LabCtrl', function ($scope, $http, $sce) {
    
    var imagesList = this;
    imagesList.polly = true;
    imagesList.images = [];

    imagesList.getNew = function() {

        $http({method: 'GET',url: '/api/images',headers: {
            'x-circuit-breaker': imagesList.polly
          }}).then(function(response){
            var result = Math.floor((Math.random() * 3) + 1);
            var style = 'adjective';
            
            if(result == 2)
                style = 'noun';
            else if(result == 3)
                style = 'verb';
            
            imagesList.images.push(
                {title:response.data.title, content: $sce.trustAsHtml(response.data.content), link: response.data.link, style: style});
            
        },function (data) {
            var result = Math.floor((Math.random() * 3) + 1);
            var style = 'adjective';

            if(result == 2)
                style = 'noun';
            else if(result == 3)
                style = 'verb';

            imagesList.images.push(
                {title:"Error", content: "", link: "", style: style});
        });
    };
});
